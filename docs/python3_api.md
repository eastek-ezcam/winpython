# Python3 API
## Version: 1.0.0

* [Overview](#overview)
* [Pre-requisites](#pre-requisites)
* [Package](#package)
* Classes
* Examples
* 


---

<h1 id="overview">Overview</h2>

---

<h1 id="pre-requisites">Pre-requisites</h2>

---

<h1 id="package">Package</h1>





# Methods

## ezcam

所有 API 都在模組 `ezcam` 中。下面介紹所有可用的 function 。

### JOB()

#### 功能

取得當前 ezCAM 正在開啟的 Job 名稱。

#### 參數

無。

#### 回傳值

正在開啟的 Job 名稱。如果沒有開啟 Job ，將回傳空字串。

#### 參數

無。

#### 範例

```python
import ezcam
# Show current job.
print(ezcam.JOB())
```

---

### STEP()

#### 功能

取得當前 ezCAM 正在開啟的 Step 名稱。

#### 參數

無。

#### 回傳值

正在開啟的 Step 名稱。如果沒有開啟 Step ，將回傳空字串。


#### 範例

```python
import ezcam

# Show current job.
print(ezcam.JOB())

# Show current step.
print(ezcam.STEP())
```

---

### COM

#### 功能

將 COM 指令傳送給 ezCAM 執行。

#### 參數

##### Type1

| Parameters | Description                       |
| ---------- | --------------------------------- |
| content    | COM 指令的內容，例如 `open_job, job=test` |

##### Type2

| Parameters   | Description                              |
| ------------ | ---------------------------------------- |
| Command Name | COM 指令名稱。例如`open_entity`                 |
| **kwargs     | COM 指令參數列表，例如 `job='test', type='step', name='mystep'` |



#### 回傳值

STATUS 值，0 表示正常結束，非 0 表示異常結束。

#### 範例

```python
# Type 1 example.

import ezcam
ret = ezcam.COM('open_job, job={job}'.format(job='test'))
if ret != 0:
    print('open_job failed !')
```



```python
# Type 2 example.

import ezcam
ret = ezcam.COM('open_job', job='test')
if ret != 0:
    print('open_job failed !')
```





---

### DO_INFO()

#### 功能

使用 info 指令取得當前 ezCAM 的內部資訊，具體可用的參數可參考 主畫面 > Action > Info 介面。

#### 參數


| Parameters | Description |
| ---------- | ----------- |
| args       | DO_INFO 參數  |

#### 回傳值

存放 DO_INFO 變數值的 Dictionary ，Key 是變數名稱，Value 是值。

#### 範例

```python
row_info = ezcam.DO_INFO('-t matrix -e test_job/matrix -d ROW')

# Print row name
print(row_info['gROWname'])

# Print layer type
print(row_info['gROWlayer_type'])
```



---

### dbutil

#### 功能

bin_tools/dbutil 的函式版本，詳細使用方式可參考 [bin_tools/dbutil](./bin_tools.html) 。

#### 參數


| Parameters | Type   | Description |
| ---------- | ------ | ----------- |
| args       | string | dbutil 的參數。 |

#### 回傳值

字串 list 。

#### 範例

```python
# Get DB Path: dbutil path dbs <name>
ret = ezcam.dbutil('path', 'dbs', 'mydb')

# Print dbutil return value
print(ret[0])
```



# Classes

## Job

用於表示 `Job` 的物件，使用 `ezcam.get_job('job_name')` 可取得。

### name

表示 job 的名稱。

### open()

將料號讀入記憶體，與 `COM open_job` 相同效果。

#### 範例

```python
import ezcam
job = ezcam.get_job('myjob')
# 與 "COM open_job, job=myjob" 同義
job.open()
```



### close()

將料號從記憶體中釋放，與 `COM close_job` 相同效果。

#### 範例

```python
import ezcam
job = ezcam.get_job('myjob')
job.open()
# 釋放記憶體
job.close()
```



### get_steps_list()

回傳此 job 的所有 step 列表，且每個元素的型態為 `Step` 。

#### 範例

```python
import ezcam
job = ezcam.get_job('myjob')
steps = job.get_steps_list()
for step in steps:
    # <class 'ezcam.odb.Step'>
    print(type(step))
```



### get_step(name)

取得此 job 的某個 step ，型態為 `Step` 。

#### 範例

```python
import ezcam
job = ezcam.get_job('myjob')
step = job.get_step('mystep')
# <class 'ezcam.odb.Step'>
print(type(step))
```



## Step

用於表示 Step 的物件。

### name

Step 名稱。

### open()

開啟此 Step 的 Graphic Editor 介面，與 `COM open_entity, type=step` 同義。

#### 範例

```python
import ezcam
job = ezcam.get_job('myjob')
steps = job.get_steps_list()
step = job.get_step('mystep')
step.open()       
```



### get_layers_list()

回傳此 job 的所有 layer 列表，且每個元素的型態為 `Layer` 。

```python
import ezcam
job = ezcam.get_job('myjob')
steps = job.get_steps_list()
step = job.get_step('mystep')
layers = step.get_layers_list()
for layer in layers:
    # <class 'ezcam.odb.Layer'>
    print(type(layer)) 
```



## Layer

用於表示 Layer 的物件。

### name

Layer 名稱。

### context

Layer 的 context ，數值可能有：

- board
- misc

### type

Layer 的層型態，數值可能有：

* signal
* power_ground
* mixed
* solder_mask
* silk_screen
* solder_paste
* drill
* rout
* document

### polarity

Layer 的極性，數值可能有：

* positive
* negative

### side

Layer 的面次，數值可能有：

* top
* inner
* bottom

### drl_start

### drl_end

### foil_side

### sheet_side

### get_features_list()

## Feature

### type

pad | line | arc | text | surface | barcode

### index

### attributes

### xmin, ymin xmax, ymax

### polarity

### xcenter, ycenter

### width, height

## Feature - Surface

當 `type = surface` 時，以下欄位可以使用。

### islands_count

### holes_count

### get_contours()

## Feature - Surface - Contour

### polarity

### xcenter, ycenter

### width, height

### points

## UnitsValue

可以保存帶單位的數值，並且能在各單位間轉換。

目前支援的單位：

* inch
* mm
* mil
* ml (=mil)
* micron
* my (=micron)

#### 範例

```python
v = ezcam.UnitsValue(2, 'mil') # 初始化一個 2 mil 的數值
v.get_val('mil') # 2
v.get_val('ml') # 2
v.get_val('inch') # 0.002
v.get_val('mm') # 0.0508
v.get_val('micron') # 50.8
v.get_val('my') # 50.8
```



## UnitsPoint

帶有 x, y 兩個 `UnitsValue` 可用來表示平面座標。